#Path setup
PATH=/usr/bin:$PATH
stty -ixon

#Prompt format
PS1="\\[\\033[36m\\]\\u@\\h\\[\\033[0m\\]:\\[\\033[35m\\]\\w \\[\\033[0m\\]\[\033(0\]b\[\033(B\] "

#Google test environment
export GTEST_PATH=~/libs/googletest
