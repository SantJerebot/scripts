"General
set mouse=a
set noswapfile
set tabstop=4
set shiftwidth=4

"General mappings
nnoremap <c-s> :w<CR>

"Pathogen
execute pathogen#infect()
call pathogen#helptags() "generate helptags for everything in runtimepath
syntax on
filetype plugin indent on
 
"NERDTree
autocmd vimenter * NERDTree
autocmd VimEnter * wincmd p
let NERDTreeShowHidden=1
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
nnoremap <F8> :NERDTreeToggle<CR>

"BufTabLine
nnoremap <s-l> :bnext<CR>
nnoremap <s-h> :bprevious<CR>
nnoremap <s-j> :BD<CR>

" Airline
set laststatus=2 " Always display status line
let g:airline_powerline_fonts = 1
let g:Powerline_symbols = 'fancy'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline_theme='solarized'
let g:airline_solarized_bg = 'dark'
set t_Co=256

" FSwitch
nnoremap <s-f> :FSRight<CR>
