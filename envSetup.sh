#########################
#   INSTALL DEV TOOLS   #
#########################
echo "INSTALLING DEV TOOLS"
#install git
apt-cyg install git
#install gcc
apt-cyg install gcc-g++
#install make
apt-cyg install make
#install cmake
apt-cyg install cmake
#install ruby
apt-get install rubygems

######################### 
#    CONFIGURE BASH     #
#########################
echo "DOWNLOADING ENVIRONMENT CONFIG FILES"
git clone https://SantJerebot@bitbucket.org/SantJerebot/scripts.git
cp ~/scripts/.profile .profile
cp ~/scripts/.bashrc .bashrc
cp ~/scripts/.vimrc .vimrc
source ~/.bashrc

#####################
#   CONFIGURE VIM   #
#####################
echo "CONFIGURING VIM"

#configure curl certificates
echo ".curl"
echo 'cacert=/etc/ssl/certs/ca-bundle.crt' > ~/.curl

#install pathogen
echo ".pathogen"
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

#ctrlp
echo ".ctrlp"
git clone https://github.com/ctrlpvim/ctrlp.vim.git ~/.vim/bundle/ctrlp.vim

#install NERDTree
echo ".NERDTree"
git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree

#airline
echo ".Airline"
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline
git clone https://github.com/vim-airline/vim-airline-themes ~/.vim/bundle/vim-airline-themes

#supertab
echo ".Supertab"
git clone https://github.com/ervandew/supertab.git ~/.vim/bundle/supertab

#bufkill
echo ".BufKill"
git clone https://github.com/qpkorr/vim-bufkill ~/.vim/bundle/vim-bufkill

#FSwitch
echo ".FSwitch"
git clone https://github.com/derekwyatt/vim-fswitch.git ~/.vim/bundle/fswitch

############################
#    INSTALL UNITY TEST    #
############################
echo "INSTALL UNITY TEST FRAMEWORK"
git clone https://github.com/ThrowTheSwitch/Unity.git ~/testing/Unity
git clone https://github.com/ThrowTheSwitch/CMock.git ~/testing/CMock

#Install ceedling
gem install ceedling
echo "alias ceedling=~/bin/ceedling" >> ~/.bashrc
source ~/.bashrc

############################
#    INSTALL UNITY TEST    #
############################
#echo "INSTALL GOOGLE TEST"
#mkdir ~/libs
#git clone https://github.com/google/googletest.git ~/libs/googletest
#mkdir ~/libs/googletest/build
#cd ~/libs/googletest/build
#cmake .. -Dgtest_disable_pthreads=ON
#make

#####################
#        END        #
#####################
echo "Installation complete!"
